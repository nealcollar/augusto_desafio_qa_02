#language:pt

Funcionalidade: Historia de validacao de requisicao
    Para realizar um Post com parametros e um Get para a API. 
    Sendo um tester.
    Posso enviar e validar as requisicoes.


Cenario: Fazendo requisicao Post
	Dado que estabeleci uma requisicao Post com os dados id: 1, "user_name" e "password"
	Entao esperar a resposta e validar o status.

Cenario: Fazendo requisicao Get 
	Dado que estabeleci uma requisicao GET e peguei a informacao
	Entao validar status code e exibir 
