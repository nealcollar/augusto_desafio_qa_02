Dado("que estabeleci uma requisicao Post com os dados id: {int}, {string} e {string}") do |id, name, password|
    @url_rest = UrlRest.new
    url = @url_rest.select_url_post()
    user = @url_rest.estruct_user_post(id, name, password)
    @result_post = @url_rest.post(url, user) 
end

Entao("esperar a resposta e validar o status.") do
    expect(@result_post.code).to eq(200)
end

Dado("que estabeleci uma requisicao GET e peguei a informacao") do
    @url_rest = UrlRest.new
    url = @url_rest.select_url_get()
    @results_get = @url_rest.get(url)
end

Entao("validar status code e exibir") do
    expect(@results_get.code).to eq(200)
    results_formated = @url_rest.format_json(@results_get)
    @url_rest.print_id_completed(results_formated)
end