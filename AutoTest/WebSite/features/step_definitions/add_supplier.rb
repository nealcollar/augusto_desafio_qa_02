Dado("que eu entrei na pagina de login") do
  visit  'http://www.phptravels.net/admin'
end
  
  Quando("eu faco login com {string} e {string}") do |email, senha|
    @home = HomeBases.new
    @home.fazer_login(email, senha)
  end
  
  Quando("entrei na pagina em accounts e suppliers para add novo supplier") do
    @home.add_supplier()
  end
     
  Quando("add novo supplier com {string}, {string}, {string}, {string} e {string}") do |firstName, lastName, email, name, password|
    @home.supplier_data(firstName, lastName, email, name, password)
  end
  
  Entao("deve ser criado com sucesso o novo supplier") do
    expect(page).to have_content "augustos_cesar@hotmail.com.br"
  end