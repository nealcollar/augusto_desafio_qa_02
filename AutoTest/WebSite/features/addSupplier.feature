#language:pt

Funcionalidade: criar um novo Supplier
	Para que eu possa cadastrar e gerenciar me empreendimento.
	Sendo um usuario.
	Posso cadastrar meu novo Supplier.

Contexto: Pagina inicial
	Dado que eu entrei na pagina de login

Cenario: Cadastrar novo Supplier

	Quando eu faco login com "admin@phptravels.com" e "demoadmin"
	E entrei na pagina em accounts e suppliers para add novo supplier
	Quando add novo supplier com "Augusto", "Cesar", "augustos_cesar@hotmail.com.br", "Demo" e "123456"
	Entao deve ser criado com sucesso o novo supplier
