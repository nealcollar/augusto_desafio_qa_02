
class HomeBases
  include Capybara::DSL

  #criando estrutura para informações necessarias do Login 
  def element_log 
    {
      email: 'input[name=email]',
      password: 'input[name=password]',
      button_login: 'Login'
    }
  
  end

  #criando estrutura para informações necessarias par um novo Supplier
  def elements_suppliers 
    {
      firstName: 'input[name=fname]',
      lastName: 'input[name=lname]',
      email: 'input[name=email]',
      name: 'input[name=itemname]',
      password: 'input[name=password]',
    }
  
  end
  

  def fazer_login(email, senha)
    find(element_log[:email]).set email
    find(element_log[:password]).set senha
    click_button(element_log[:button_login])
  end

  #navega pela API ate Account->Suplier e clica no botao "add"
  def add_supplier
    sleep 1
    click_link('Accounts') 
    click_link('Suppliers')
    click_button('Add')
  end

  def supplier_data(firstName,lastName,email,name,password)
    find(elements_suppliers[:firstName]).set firstName
    find(elements_suppliers[:lastName]).set lastName
    find(elements_suppliers[:email]).set email
    find(elements_suppliers[:name]).set name
    find("option[value='BR']").click
    find(elements_suppliers[:password]).set password
    click_button('Submit')
  end
  
end