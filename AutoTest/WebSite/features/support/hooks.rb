
Before do
    page.current_window.resize_to(1280, 800)
end

#tirar print para comprovação do teste
After do |scenario|
    nome_cenario = scenario.name.gsub(' ','_').downcase!
    screenshot = "log/screenshots/#{nome_cenario}.png"
    page.save_screenshot(screenshot)
    embed(screenshot, 'imagem/png', 'Clique aqui para ver o print do teste.')
end
