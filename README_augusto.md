##Orientações da criação do ambiente
* **Instalando Ruby

        - Como utilizo windows fico atrelado a duas opções 1ª - railsinstaler 2ª - bitnamipack(ruby), como já tive experiência nessa instalação de ambiente opto pela qual melhor se mostrou eficiente pra mim ao longo dos projetos que trabalhei, dito isso minha escolha é a 2ª, pois achei mais organizada e otimizada com tudo que o packge oferece.

        - Além do bitnamipack(ruby), instalei o sourcetree para poder clonar o repositório na minha maquina e poder manipular da melhor forma.

        - Também tive que instalar o chrome-driver para poder instanciar o navegador durante os testes

        - Já para edição eu utilizei dois programas que gosto muito o "sublime" e o "microsoft visual studio"

* **Executando testes

        - Apos toda a preparação do ambiente pude começar a explorar mais sobre essa área da automação, comecei estruturando da melhor forma que aprendi nesses dias as pastas e os arquivos, junto com as facilitações que cada gem me ofereceu.

        - Quando estruturei os arquivos comecei a fazer um BDD simples de acordo com o cenário(como pode ver nos documentos em word junto com o projeto), após ter entendido parte do conceito e exercer meu aprendizado fui para codificação.

        - Nesta parte encontrei os grandes problemas, pois meu conhecimento sobre as bibliotecas foi desafiado, pois em muitos casos não sabia que ferramenta usar devido a ser uma nova experiencia para mim, porem consegui realizar um trabalho com 200% do meu esforço, porém ainda não fiquei totalmente satisfeito com o resultado, mas preciso entregar para que vocês possam me avaliar, e quem sabe aprimorar essas novas habilidades com vocês no futuro.

        - Para executar os testes do meu código utilizei muito o "irb" e os próprios recursos do Cucumber.

        - Essa foi a experiencia que obtive após concluir o primeiro e o segundo desafio.
